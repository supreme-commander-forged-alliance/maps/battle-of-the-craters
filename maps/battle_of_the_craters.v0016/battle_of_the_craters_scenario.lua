version = 3
ScenarioInfo = {
    name = 'Battle of the Craters',
    description = 'A battle simulation that has been used throughout the ages. \r\n\r\nMap designed from scratch by (Jip) Willem Wijnia. \r\nInspired by the map Astro Crater by BRS_Astro.\r\n\r\nYou can find the latest version at: https://gitlab.com/supreme-commander-forged-alliance/maps',
    type = 'skirmish',
    starts = true,
    preview = '',
    size = {512, 512},
    map_version = 16,
    map = '/maps/battle_of_the_craters.v0016/battle_of_the_craters.scmap',
    save = '/maps/battle_of_the_craters.v0016/battle_of_the_craters_save.lua',
    script = '/maps/battle_of_the_craters.v0016/battle_of_the_craters_script.lua',
    norushradius = 120.000000,
    norushoffsetX_ARMY_1 = 0.000000,
    norushoffsetY_ARMY_1 = 0.000000,
    norushoffsetX_ARMY_2 = 0.000000,
    norushoffsetY_ARMY_2 = 0.000000,
    norushoffsetX_ARMY_3 = 0.000000,
    norushoffsetY_ARMY_3 = 0.000000,
    norushoffsetX_ARMY_4 = 0.000000,
    norushoffsetY_ARMY_4 = 0.000000,
    norushoffsetX_ARMY_5 = 0.000000,
    norushoffsetY_ARMY_5 = 0.000000,
    norushoffsetX_ARMY_6 = 0.000000,
    norushoffsetY_ARMY_6 = 0.000000,
    Configurations = {
        ['standard'] = {
            teams = {
                { name = 'FFA', armies = {'ARMY_1','ARMY_2','ARMY_3','ARMY_4','ARMY_5','ARMY_6',} },
            },
            customprops = {
                ['ExtraArmies'] = STRING( 'NEUTRAL_CIVILIAN' ),
            },
        },
    }}
