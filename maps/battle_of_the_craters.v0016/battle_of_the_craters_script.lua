local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

function OnPopulate()

  if(ScenarioInfo.Options['PrebuiltUnits']) == "On" then
    CreatePrebuilt();
    ScenarioInfo.Options['PrebuiltUnits'] = "Custom";
    LOG(ScenarioInfo.Options['PrebuiltUnits']);
  end

  ScenarioUtils.InitializeArmies()

  local faction = Random(1, 3);
  local factionName = ConvertToFaction(faction);

  LOG("Neutral status: "..ScenarioInfo.Options.CivilianAlliance);
  LOG("Neutral faction: "..factionName);

  if ScenarioInfo.Options.CivilianAlliance == "removed" then
      -- Create the unit group as wreckage.
    local unitGroup = ScenarioUtils.CreateArmyGroup("NEUTRAL_CIVILIAN", factionName);
    CreateWreckage(unitGroup, 0.3);
  else
      -- Creating the unit group.
    local unitGroup = ScenarioUtils.CreateArmyGroup("NEUTRAL_CIVILIAN", factionName);
      -- Randomly damaging it.
    for num,unit in unitGroup do
      unit:SetHealth( unit, Random(( unit:GetMaxHealth() * 0.3), unit:GetMaxHealth() * 0.9 ))
    end

    SetNeutralAlliance();
  end

  -- General wreckage.
  local unitGroup = ScenarioUtils.CreateArmyGroup("NEUTRAL_CIVILIAN", factionName.."Wreckage");
  CreateWreckage(unitGroup, 0.5);

  for k, army in ArmyBrains do
       -- Vision in the middle
    ScenarioFramework.CreateVisibleAreaLocation(14, "Vision", 45, army);
  end
end

-- For some reason, this didn't happen anymore during testing. So I'm doing it here just to be sure.
function SetNeutralAlliance()
  for k, armyName in ListArmies() do
    if(armyName != "NEUTRAL_CIVILIAN") then
      SetAlliance(armyName, "NEUTRAL_CIVILIAN", ScenarioInfo.Options.CivilianAlliance);
    end
  end
end

function CreateWreckage(unitGroup, multiplier)
  for num,unit in unitGroup do
    local bp = unit:GetBlueprint()

    -- check if a multiplier was set
    if not multiplier then
      multiplier = bp.Wreckage.MassMult or 0
    end

    -- determine values
    local mass   = bp.Economy.BuildCostMass * multiplier
    local energy = bp.Economy.BuildCostEnergy * (bp.Wreckage.EnergyMult or 0)
    local time   = (bp.Wreckage.ReclaimTimeMultiplier or 1)

    -- set the values
    local wreckage = unit:CreateWreckageProp(0)
    wreckage:SetMaxReclaimValues(time, mass, energy)

    -- destroy the unit
    unit:Destroy()
  end
end

function CreatePrebuilt()
  for iArmy, Army in ArmyBrains do
    if(Army.Name != "ARMY_9" and Army.Name != "NEUTRAL_CIVILIAN") then
      faction = ConvertToFaction(Army:GetFactionIndex());
      ScenarioUtils.CreateArmyGroup(Army.Name, Army.Name .. "_" .. faction);
    end
  end
end

-- FACTIONS --
-- 1 = UEF
-- 2 = Aeon
-- 3 = Cybran
-- 4 = Seraphin (?)

local Factions = {'UEF', 'Aeon', 'Cybran', 'Seraphin'};

function ConvertToFaction(factionNumber)

  -- default to UEF in case there are nomads or TA factions
  if factionNumber > 4 then 
    factionNumber = 1
  end

  return Factions[factionNumber];
end

function OnStart(self)

end
