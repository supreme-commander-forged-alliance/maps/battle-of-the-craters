## Battle of the Craters

_A map for Supreme Commander: Forged Alliance (Forever)_

![](images/overview.png)

A common and popular map design in the community of Forged Alliance Forever. The original map lacks however all the basic aesthetical eye candy that you can have in a map. Therefore this map - created from scratch - along with various features such as being able to walk into the water with your commander.

![](images/shadows.png)

Typically terrain in Supreme Commander can not cast shadows on itself - this is a limitation of the engine. Via using World Machine we generated a map-wide lightmap that can simulate those shadows for us. The shadows are merely an albedo decal - they are not recognized as shadows by the game. Therefore units will still cast their own shadow and are not affected by these shadows in general.

![](images/lightmap.png)

An example of the lightmap that is 256x256 in-game so that the shadows are a bit blurred.

![](images/normal.png)

Next to that we use a map-wide normal map generated in World Machine to make the terrain feel more detailed than it actually is. This keeps the aesthetical eye-candy top notch without influencing the gameplay at hand. This is a 512x512 preview - the normal map used in-game has a resolution of 4096x4096.

## Statistics of the map

The map is designed to be played as a 1v1, 2v2 or a 3v3. There is reclaim at the start location such that players that are aware of reclaim can take an advantage from it. There isn't sufficient reclaim to make a real difference. There is reclaim in the center that is relevant - depending on the civilian settings the structures may be alive or destroyed when there are no civilians.

The map uses a different pre-built setting in which you start with two factories, two pgens and all four mass extractors. This gives me full control over where the pre-built units are actually situated and guarantees symmetrical properties.

## Tools

The World Machine file is compatible with World Machine Standard Build 3023 'Mt Daniel' 64 bits.

## License

All assets are licensed with [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).